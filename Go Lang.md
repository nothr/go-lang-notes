## Data Types

```Go
bool

string

int int8 int16 int32 int64
uint uint8 uint16 uint32 uint64

byte // alias for uint8

rune  //alias for int32

float32 float64

complex64 complex128
```
## Declaring Variable

```Go
var number int // value is 0
```

`var` is the keyword, `number` is the name and `int` is the datatype

### Assigning value to the variable

```Go
var num int  = 20 // usual assignment
num := 20 // Automatic assignment
```

Based on the type give it takes assigns the datatype `int` to the variable `num`
